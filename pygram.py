import Queue
import threading
import subprocess
import sys
import curses, time, traceback, sys
import curses.wrapper
import re
from Queue import Empty

def enqueue_output(out, queue, screen):
    outStr = ''
    for line in iter(out.readline, b''):
        ansi_escape = re.compile(r'\x1b[^m]*m')
        screen.addLine(ansi_escape.sub('', line))
    out.close()

class Screen():
    def __init__(self, stdscr):
        self.timer = 0
        self.statusText = "PYGRAM - a telegram Wrapper -"
        self.searchText = ''
        self.stdscr = stdscr

        # set screen attributes
        self.stdscr.nodelay(1) # this is used to make input calls non-blocking
        curses.cbreak()
        self.stdscr.keypad(1)
        curses.curs_set(0)     # no annoying mouse cursor

        self.rows, self.cols = self.stdscr.getmaxyx()
        self.lines = []

        curses.start_color()

        # create color pair's 1 and 2
        curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(2, curses.COLOR_WHITE, curses.COLOR_BLACK)

        self.paintStatus(self.statusText)

    def connectionLost(self, reason):
        self.close()

    def addLine(self, text):
        """ add a line to the internal list of lines"""

        self.lines.append(text)
        self.redisplayLines()

    def redisplayLines(self):
        """ method for redisplaying lines 
            based on internal list of lines """

        self.stdscr.clear()
        self.paintStatus(self.statusText)
        i = 0
        index = len(self.lines) - 1
        while i < (self.rows - 3) and index >= 0:
            self.stdscr.addstr(self.rows - 3 - i, 0, self.lines[index], 
                               curses.color_pair(2))
            i = i + 1
            index = index - 1
        #self.stdscr.refresh()

    def paintStatus(self, text):
        if len(text) > self.cols: raise TextTooLongError
        self.stdscr.addstr(self.rows-2,0,text + ' ' * (self.cols-len(text)), 
                           curses.color_pair(1))
        # move cursor to input line
        self.stdscr.move(self.rows-1, self.cols-1)

    def close(self):
        """ clean up """

        curses.nocbreak()
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()

p = subprocess.Popen("./telegram", stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)

curses.setupterm() 
curses.noecho()
stdscr = curses.initscr() # initialize curses
screen = Screen(stdscr)   # create Screen object
stdscr.refresh()

outQueue = Queue.Queue()

outThread = threading.Thread(target=enqueue_output, args=(p.stdout, outQueue, screen))

outThread.daemon = True
outThread.start()

commandText = ''
while True:
    screen.timer = screen.timer + 1
    c = stdscr.getch() # read a character
    
    if c == -1:
        c = ''
    elif c == curses.KEY_BACKSPACE:
        commandText = commandText[:-1]
    elif c == curses.KEY_ENTER or c == 10:
        if commandText.__len__() > 1:
            p.stdin.write('%s\n' % commandText)
            commandText = ''
    elif c > 255:
        c = ''
    else:
        commandText = commandText + chr(c)

    screen.stdscr.addstr(screen.rows-1, 0, commandText)


